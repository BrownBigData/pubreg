echo "WELCOME TO NORTHSTAR LINUX INSTALLER"
echo "installing dependencies..."
sudo yum install curl
sudo yum install wget
sudo yum install zip
sudo yum install unzip
echo "installing docker and docker-compose"
sudo yum install docker
sudo yum install docker.io
sudo yum install docker-compose
chcon -Rt svirt_sandbox_file_t ./input
chcon -Rt svirt_sandbox_file_t ./output
echo "done!"