# Getting Started
![Sample Audio](videos/create_workspace.webm)

In this tutorial, we are going to investigate the following functionalities in **Northstar**.

* Login
* Data Exploration
    * Dataset Selection and Attributes ScrollView
    * Attribute Selection and Deletion
    * Brushing
    * Data Cleaning: Filtering and Attribute Creation
* Automated Machine Learning
    * Predictions
    * Pipeline Exploration
* Data Import


## Login
First of all connect to your *Northstar* deployment on http://localhost:1234 (or with the IP of your server), then login with the provided password.

<div align="center"><img src="tutorial/login.gif"></div>

## Data Exploration

*Northstar* is an interactive data science canvas in which you can perform *data exploration* and *inference* intuitively. Any object that you place on the canvas can be *moved*, *resized* and *deleted*. You can also navigate in the canvas by clicking on an empty spot and dragging the canvas in the direction you prefer.

### Dataset Selection and Attributes ScrollView

You can select the **datasets** button on the left menu to get an overview of the currently *loaded dataset*. Once you selected a dataset from the menu you can click the **attributes** button to see its columns. Every time you select a dataset, the selection will be propagated to the attributes.

<div align="center"><img src="tutorial/scrollview.gif"></div>

### Attribute Selection and Deletion

To produce the **histogra**m of an attribute just select it from the scrollview and drag it on the canvas. You can dynamically **resize** the size of the windows by clicking the blue triangle in the bottom right corner of the view. To delete any view just *right click on it* or, if you want to delete multiple views at once, keep pressing *CTRL* to activate the *erase function* and scribble over the views that you would like to erase.

<div align="center"><img src="tutorial/deletion_drag.gif"></div>


### Brushing and Normalization

**Brushing** is a powerful visualization that allows fast filtering between views. To perform *brushing*, first of all, place the views of interest one next to the other until the border of the view are *highlighted* in yellow. Now the two views are connected, you can select any bin (or groups of bins) in any of the connected views and the system will propagate this action on the other. *Brushing* is bidirectional and synchronous so that you can perform mutual brushing. 

Often it is interesting to combine brushing with attribute **normalization** to get a better understanding of the proportion of selected values: it is possible to perform normalization by double-clicking on the *x-axis*.

<div align="center"><img src="tutorial/brushing.gif"></div>

### Attribute Creation: Filtering and Data Cleaning

In *Northstar*, it is possible to perform data cleaning with simple drag-and-drop gestures. In the animation below we clean the attribute *age* by using **Filtering** and **Injection**. To perform filtering just click on the bin that you would like to use and then drag the target button in the top right corner in an empty spot on the canvas. To perform *injection* instead perform the same action but dragging the target button on another compatible view.

<div align="center"><img src="tutorial/cleaning.gif"></div>

## Automated Machine Learning

### Predictions

At this point, you are ready to deploy state-of-the-art machine learning workflows using **Northstar's automated machine learning** operator. To build an *automl optimizer* on the currently selected dataset just click on **operators/algorithms/predictor** and drag it on the canvas. Now you can select your target variable from the *attributes views* by dragging it over the **target** square of the *predictor* operator: the *optimizer* will immediately start looking to for the best machine learning workflow to solve your data problem. 

*Northstar* is an interactive platform, and the optimizer is by no means different: it will immediately return currently top performing solutions in a way that you can immediately start to explore them.  

<div align="center"><img src="tutorial/automl.gif"></div>


### Pipeline Exploration and Script Export

Once you are satisfied with a workflow, just select one of the returned pipeline in the right side of the *predictor* operator by drag it on an empty spot in the canvas. *Northstar* will create a new view to let you explore the internal structure of the selected pipeline: by clicking on the **stages** tab you can explore the workflow architecture and each component hyperparameters. Then by clicking on the **arrow** in the bottom right corner, you can generate the python script of the freshly found solution ready for deployment. Finally by clicking on the **features** tab you can investigate the *features predictive-power* (features relevance) for the requested task.
 
<div align="center"><img src="tutorial/pipeline.gif"></div>

## Data Import

You can import your dataset from a *.csv* file or from a *Google Spreadsheet*. In case you want to use a *.csv* file, select it from your OS file manager and drag it in the canvas. Instead, if you want to import your data from a *Google Spreadsheet*, press **CTRL+A** to select all the elements in the table, **CTRL+C** to copy them and then **CTRL+V** in *Northstar*. In both cases, the system will provide you a preview of the imported dataset and will ask you to assign a name to it. To start using your dataset go into the *dataset* scrollview and select it.

<div align="center"><img src="tutorial/import.gif"></div>
