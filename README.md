# Northstar

## Installation

First of all, you need to clone this repository. 

You can do this using the git command:  `git clone https://gitlab.com/BrownBigData/pubreg.git` or using the gitlab download functionality (click on the cloud symbol in the top right next to *web IDE*)
### Redhat

1. In the project folder, run `redhat_INSTALL.sh`. This will install all required dependencies, including Docker and the Northstar Docker images.
2. Run `docker-compose up`
3. Launch `http://localhost:1234` in your web-browser (replace `localhost` with an external IP address if necessary and be sure that your networking configurations allow external access)

### Ubuntu 18.04

1. In the project folder, run `sudo ubuntu_INSTALL.sh`. This will install all required dependencies, including Docker and the Northstar Docker images.
2. Run `sudo docker-compose up`
3. Launch `http://localhost:1234` in your web-browser (replace `localhost` with an external IP address if necessary and be sure that your networking configurations allow external access)

### Windows Desktop
1. Download and install Docker from https://docs.docker.com/v17.09/docker-for-windows/install
1. Make sure you assign at least 4G of memory to Docker through the Docker settings
2. Open an Administrator Console in the project folder
3. Run `docker-compose up`
4. Launch `http://localhost:1234` in your web-browser


## Configuration
By default, Northstar uses only 4G of memory and 4 CPUs. This can be configured in `docker-compose.yaml`

## Update

To pull Northstar's latest version run the following commands:

`sudo docker-compose stop`

`sudo docker-compose rm -f`

`sudo docker-compose pull`  

`sudo docker-compose up`

## Getting Started

Please, take your time to read *Northstar's* getting started [**interactive tutorial**](https://gitlab.com/BrownBigData/pubreg/blob/master/tutorial.md) to become familiar with the platform's functionalities. 

## Compatibility
We currently support Google Chrome and Mozilla Firefox.
